import React from 'react';
import { Todo } from '../types/Todo';
import styles from '../styles/styles.module.css';


interface TodoListProps {
  todos: Todo[];
  onDeleteTodo: (id: number) => void;
}

const TodoList: React.FC<TodoListProps> = ({ todos, onDeleteTodo }) => {
  return (
    <ul className={styles['todo-list']}>
      {todos.map((todo) => (
        <li className={styles['todo-list-item']} key={todo.id}>{todo.text}
        <button className={styles['delete-button']} onClick={() => onDeleteTodo(todo.id)}>Delete</button>
        </li>
      ))}
    </ul>
  );
};

export default TodoList;
