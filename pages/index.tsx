import React from 'react';
import TodoApp from '../components/TodoApp';

const HomePage: React.FC = () => {
  return <TodoApp />;
};

export default HomePage;
